# source : https://github.com/IntelRealSense/librealsense/tree/master/wrappers/python#numpy-integration

# First import the library
import pyrealsense2 as rs
import numpy as np

# Create a context object. This object owns the handles to all connected realsense devices
pipeline = rs.pipeline()
pipeline.start()

try:
    while True:
        # Create a pipeline object. This object configures the streaming camera and owns it's handle
        frames = pipeline.wait_for_frames()
        depth = frames.get_depth_frame()
        if not depth: continue

        rgb_image = np.asanyarray(frames.as_frame().get_data())
        depth_image = np.asanyarray(depth.as_frame().get_data())

        print(rgb_image.shape)
        print(depth_image.shape)

        break

finally:
    pipeline.stop()