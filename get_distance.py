import pyrealsense2 as rs

pipeline = rs.pipeline()

pipeline.start()

try :
    while True:
        frameset = pipeline.wait_for_frames()
        depth = frameset.get_depth_frame()

        w = depth.get_width()
        h = depth.get_height()
    
        d = depth.get_distance(w//2, h//2)
        
        print(w,h,d)
finally :
    pipeline.stop()