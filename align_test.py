

# First import the library
import pyrealsense2 as rs
# Import Numpy for easy array manipulation
import numpy as np
# Import OpenCV for easy image rendering
import cv2
from time import perf_counter

# Create a pipeline
pipeline = rs.pipeline()

# Create a config and configure the pipeline to stream
# different resolutions of color and depth streams
config = rs.config()

# Get device product line for setting a supporting resolution
pipeline_wrapper = rs.pipeline_wrapper(pipeline)
pipeline_profile = config.resolve(pipeline_wrapper)
device = pipeline_profile.get_device()
device_product_line = str(device.get_info(rs.camera_info.product_line))

D_HEIGHT = 720
D_WIDTH = 1280
C_HEIGHT = 240
C_WIDTH = 424
config.enable_stream(rs.stream.depth, D_WIDTH, D_HEIGHT, rs.format.z16, 30)
config.enable_stream(rs.stream.color, C_WIDTH, C_HEIGHT, rs.format.bgr8, 30)

# Start streaming
profile = pipeline.start(config)

# Getting the depth sensor's depth scale (see rs-align example for explanation)
# デバイスからdepthセンサだけを取ってくる便利な関数？
depth_sensor = profile.get_device().first_depth_sensor()
# メートル単位に変換するためのやつ．
depth_scale = depth_sensor.get_depth_scale()
print("Depth Scale is: " , depth_scale)

# We will be removing the background of objects more than clipping_distance_in_meters meters away
clipping_distance_in_meters = 0.5 #1 meter
clipping_distance = clipping_distance_in_meters / depth_scale

# Create an align object
# rs.align allows us to perform alignment of depth frames to others frames
# The "align_to" is the stream type to which we plan to align depth frames.
align_to = rs.stream.color
align = rs.align(align_to)

t = perf_counter()

# Streaming loop
try:
    while True:
        # Get frameset of color and depth
        frames = pipeline.wait_for_frames()

        depth_frame = frames.get_depth_frame()

        # Get aligned frames
        aligned_frames = align.process(frames)
        aligned_depth_frame = aligned_frames.get_depth_frame() 
        color_frame = aligned_frames.get_color_frame()

        # Validate that both frames are valid
        if not aligned_depth_frame or not color_frame:
            continue

        aligned_depth_image = np.asanyarray(aligned_depth_frame.get_data())
        # https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
        depth_image = cv2.resize(np.asanyarray(depth_frame.get_data()), (C_WIDTH, C_HEIGHT), cv2.INTER_AREA)
        color_image = np.asanyarray(color_frame.get_data())

        aligned_depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(aligned_depth_image, alpha=0.03), cv2.COLORMAP_JET)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

        upper_images = np.hstack((color_image, aligned_depth_colormap))
        under_images = np.hstack((np.zeros((C_HEIGHT, C_WIDTH, 3), dtype=np.uint8), depth_colormap))
        images = np.vstack((upper_images, under_images))
        cv2.imshow('Align Example', images)

        key = cv2.waitKey(1)
        # Press esc or 'q' to close the image window
        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break

        print(1/(perf_counter()-t))
        t = perf_counter()

finally:
    pipeline.stop()