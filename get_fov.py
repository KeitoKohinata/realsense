# First import the library
import pyrealsense2 as rs
# Import Numpy for easy array manipulation
import numpy as np
# Import OpenCV for easy image rendering
import cv2

# 
pipeline = rs.pipeline()
config = rs.config()

# 16:9
D_WIDTH = 1280
D_HEIGHT = 720
# 16:9
C_WIDTH = 640
C_HEIGHT = 360
# たまたま比率が同じだけで，違くても良い

config.enable_stream(rs.stream.depth, D_WIDTH, D_HEIGHT, rs.format.z16, 30)
config.enable_stream(rs.stream.color, C_WIDTH, C_HEIGHT, rs.format.bgr8, 30)

# Start streaming
profile = pipeline.start(config)

# depth camera の映像を color camera に揃えるためのオブジェクトを用意
align_to = rs.stream.color
align = rs.align(align_to)

# rs2_fov が返す結果とだいたい同じ結果になるように計算してみた
# 基本は rs2_fov を使おう
def get_fov(p):
    import math
    c = p.get_stream(rs.stream.color, -1).as_video_stream_profile().get_intrinsics()
    d = p.get_stream(rs.stream.depth, -1).as_video_stream_profile().get_intrinsics()
    print("color", c.ppx, c.ppy, c.width, c.height, c.fx, c.fy, c.coeffs, c.model)
    print("depth", d.ppx, d.ppy, d.width, d.height, d.fx, d.fy, d.coeffs, d.model)
    c_fov = rs.rs2_fov(c)
    d_fov = rs.rs2_fov(d)
    print(f"{c_fov=}")
    print(f"{d_fov=}")

    my_c_fov_y = 180*2*math.atan((c.height)/(2*c.fy))/math.pi
    my_c_fov_x = 180*2*math.atan((c.width)/(2*c.fx))/math.pi
    my_d_fov_y = 180*2*math.atan((d.height)/(2*d.fy))/math.pi
    my_d_fov_x = 180*2*math.atan((d.width)/(2*d.fx))/math.pi
    print(f"{my_c_fov_x=}")
    print(f"{my_c_fov_y=}")
    print(f"{my_d_fov_x=}")
    print(f"{my_d_fov_y=}")


# ストリーミング開始
try:
    get_fov(profile)

    while True:
        # Get frameset of color and depth
        frames = pipeline.wait_for_frames()

        depth_frame = frames.get_depth_frame()

        # Get aligned frames
        aligned_frames = align.process(frames)
        aligned_depth_frame = aligned_frames.get_depth_frame() 
        color_frame = aligned_frames.get_color_frame()

        # Validate that both frames are valid
        if not aligned_depth_frame or not color_frame:
            continue

        aligned_depth_image = np.asanyarray(aligned_depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        aligned_depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(aligned_depth_image, alpha=0.03), cv2.COLORMAP_JET)

        images = np.hstack((color_image, aligned_depth_colormap))
        cv2.imshow('Align Example', images)

        key = cv2.waitKey(1)
        # Press esc or 'q' to close the image window
        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break

finally:
    pipeline.stop()