## License: Apache 2.0. See LICENSE file in root directory.
## Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

###############################################
##      Open CV and Numpy integration        ##
###############################################

# %%
import pyrealsense2 as rs
import numpy as np
import cv2



# Configure depth and color streams
# opencv で言う videocapture みたいなやつ
pipeline = rs.pipeline()
# pipline に対して，カメラの設定を変えるように要求するクラス
config = rs.config()

# Get device product line for setting a supporting resolution
# 解像度を設定するために，利用できる全てのデバイスに関する情報を取得する？
pipeline_wrapper = rs.pipeline_wrapper(pipeline)
# 利用可能な1つのデバイスに関する情報を取得する？
# cf : https://intelrealsense.github.io/librealsense/python_docs/_generated/pyrealsense2.pipeline_profile.html#pyrealsense2.pipeline_profile
pipeline_profile = config.resolve(pipeline_wrapper)
# デバイスを利用できる状態にしている？
device = pipeline_profile.get_device()
device_product_line = device.get_info(rs.camera_info.product_line)

# RGB カメラが在るかチェック
found_rgb = False
for s in device.sensors:
    if s.get_info(rs.camera_info.name) == 'RGB Camera':
        found_rgb = True
        break
if not found_rgb:
    print("The demo requires Depth camera with Color sensor")
    exit(0)

# Depth カメラのストリーム設定
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

# Color カメラのストリーム設定
if device_product_line == 'L500':
    config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8, 30)
else: # 例えば D400 
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)

try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        # depth と color のまとまったフレームが来るのを待つ？
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        # 片方しか取得てきていない場合は繰り返し
        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        # array, asarray, asanyarray があるが， asanyarray は ndarray の subclass まで無視するため，最もゆるい制限の関数
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.1), cv2.COLORMAP_JET)

        depth_colormap_dim = depth_colormap.shape
        color_colormap_dim = color_image.shape

        # If depth and color resolutions are different, resize color image to match depth image for display
        # 深さ画像と色画像の解像度が異なるのであれば，カラー画像をリサイズして深さ画像に合わせる
        if depth_colormap_dim != color_colormap_dim:
            resized_color_image = cv2.resize(color_image, dsize=(depth_colormap_dim[1], depth_colormap_dim[0]), interpolation=cv2.INTER_AREA)
            images = np.hstack((resized_color_image, depth_colormap))
        else:
            images = np.hstack((color_image, depth_colormap))

        # Show images
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        cv2.waitKey(1)

finally:

    # Stop streaming
    pipeline.stop()