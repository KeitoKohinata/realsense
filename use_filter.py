# %%
import numpy as np
import pyrealsense2 as rs
import cv2

config = rs.config()
config.enable_device_from_file("bag/20230419_182853.bag")
pipeline = rs.pipeline()
profiles = pipeline.start(config)

d_filter = rs.decimation_filter()
d_filter.set_option(rs.option.filter_magnitude, 3)
h_filter = rs.hole_filling_filter()
h_filter.set_option(rs.option.holes_fill, 1)

align = rs.align(rs.stream.color)

try : 
    _frames = pipeline.wait_for_frames()
    _depth = _frames.get_depth_frame()
    a_depth = align.process(_frames).get_depth_frame()

    for i,depth in enumerate([_depth, a_depth]):
        h_depth = h_filter.process(depth).as_depth_frame()
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(np.asanyarray(h_depth.get_data()), alpha=0.05), cv2.COLORMAP_JET)
        cv2.imshow(str(i), depth_colormap)

    cv2.waitKey()
    cv2.destroyAllWindows()
finally :
    pipeline.stop()

# %%
print(depth.get_distance(650, 278))
# +2 は パディング分
print(d_depth.get_distance(217+2, 93))
print(h_depth.get_distance(217+2, 93))



