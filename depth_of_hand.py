# 指の先とカメラの距離を出すプログラム

# %%
import mediapipe as mp
from mediapipe.tasks import python
from mediapipe.tasks.python import vision
import cv2
import pyrealsense2 as rs
import numpy as np

def draw_landmarks(img, lms):
    h,w = img.shape[:2]
    for lm in lms:
        c = (int(w*lm.x), int(h*lm.y))
        cv2.circle(img, c, 4, (0,255,0), -1)

def draw_handpoint(img , lms, depth, hp=9):
    h,w = img.shape[:2]
    d_list = []
    try :
        for lm in lms:
            if lm.x < 0 or lm.y < 0: return
            c = (int(w*lm.x), int(h*lm.y))
            d = depth.get_distance(*c)
            if d != 0: d_list.append(d)
    except RuntimeError:
        print(c)
    if d_list == []:
        return
    d_list.sort()
    d = d_list[len(d_list)//2]
    c = (int(w*(lms[0].x+lms[hp].x)/2), int(h*(lms[0].y+lms[hp].y)/2))
    cv2.circle(img, c, 5, (255,255,0), -1)
    cv2.putText(img, f"{d:.2f}", c, cv2.FONT_HERSHEY_COMPLEX, 2, (0,255,0), 2, cv2.LINE_AA)

# def get_fov


base_options = python.BaseOptions(model_asset_path='mp/hand_landmarker.task')
options = vision.HandLandmarkerOptions(base_options=base_options, num_hands=1)
detector = vision.HandLandmarker.create_from_options(options)

pipeline = rs.pipeline()
config = rs.config()
# 16:9
D_WIDTH = 1280
D_HEIGHT = 720
# 16:9
C_WIDTH = 640
C_HEIGHT = 360
config.enable_stream(rs.stream.depth, D_WIDTH, D_HEIGHT, rs.format.z16, 30)
config.enable_stream(rs.stream.color, C_WIDTH, C_HEIGHT, rs.format.bgr8, 30)

align_to = rs.stream.color
align = rs.align(align_to)

# Start streaming
profile = pipeline.start(config)

# Streaming loop
try:
    while True:
        # Get frameset of color and depth
        frames = pipeline.wait_for_frames()
        aligned_frames = align.process(frames)
        depth_frame = aligned_frames.get_depth_frame() 
        color_frame = aligned_frames.get_color_frame()

        # Validate that both frames are valid
        if not depth_frame or not color_frame:
            continue

        depth_image = np.asarray(depth_frame.get_data())
        bgr_image = np.asarray(color_frame.get_data())
        rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)

        mp_image = mp.Image(
            image_format=mp.ImageFormat.SRGB,
            data=rgb_image
        )
        detection_result = detector.detect(mp_image)
        lmss = detection_result.hand_landmarks
        # annotated_image = mp.drawing_utils.draw_landmarks(mp.image.numpy_view(), detection_result)
        # cv2.imshow(cv2.cvtColor(annotated_image, cv2.COLOR_RGB2BGR))
        if lmss != []:
            # draw_landmarks(bgr_image, lmss[0])
            draw_handpoint(bgr_image, lmss[0], depth_frame)
        cv2.imshow('depth of my hand', bgr_image)

        key = cv2.waitKey(1)
        # Press esc or 'q' to close the image window
        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break

finally:
    pipeline.stop()
    cv2.destroyAllWindows()
# %%
