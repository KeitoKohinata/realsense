# 使用法 : python3 save_record.py <file_name>.bag

import pyrealsense2 as rs
import cv2
import sys
import numpy as np


D_HEIGHT = 720
D_WIDTH = 1280
C_HEIGHT = 360
C_WIDTH = 640
RESIZE_TO = (640, 360)

def main():
    file_name = sys.argv[1] if len(sys.argv)>1 else "test.bag"

    config = rs.config()
    config.enable_stream(rs.stream.depth, D_WIDTH, D_HEIGHT, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, C_WIDTH, C_HEIGHT, rs.format.bgr8, 30)
    config.enable_record_to_file(file_name)

    align_to = rs.stream.color
    align = rs.align(align_to)

    pipeline = rs.pipeline()
    profiles = pipeline.start(config)

    try:
        key = -1
        while key==-1:
            frames = align.process(pipeline.wait_for_frames())

            depth_image = cv2.resize(np.asanyarray(frames.get_depth_frame().get_data()), RESIZE_TO, cv2.INTER_AREA)
            color_image = np.asanyarray(frames.get_color_frame().get_data())

            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            images = np.hstack((color_image, depth_colormap))
            cv2.imshow('images', images)

            key = cv2.waitKey(1)
    finally:
        cv2.destroyAllWindows()
        pipeline.stop()


if __name__=="__main__":
    main()