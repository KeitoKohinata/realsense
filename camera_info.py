# デバイス，センサ情報を表示するサンプル

import numpy as np
import cv2
import pyrealsense2 as rs

RS_INFO_LIST = [k for k,v in rs.camera_info.__dict__.items() if isinstance(v,rs.pyrealsense2.camera_info)]

def display_info(x):
    """
    x が持つ情報を表示する．

    Parameters
    ---
    `x` : device or sensor \n
    """
    print(f"{x.get_info(rs.camera_info.name)}")
    for i in range(1,len(RS_INFO_LIST)):
        i = (rs.pyrealsense2.camera_info)(i)
        if x.supports(i):
            str_i = f"{i}"[12:]
            print(f"  {str_i}  {x.get_info(i)}")
            # print(f"    {x.get_info(i)}")

pipeline = rs.pipeline()
config = rs.config()

pipeline_wrapper = rs.pipeline_wrapper(pipeline)
pipeline_profile = config.resolve(pipeline_wrapper)
device = pipeline_profile.get_device()

print("# device info #")
display_info(device)
print()

print("# sensor info #")
for s in device.sensors:
    display_info(s)
    

