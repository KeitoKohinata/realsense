# 不可能です！
# https://github.com/IntelRealSense/librealsense/issues/7122



# %%
import pyrealsense2 as rs

# video から読み取るための処理
config = rs.config()
config.enable_device_from_file("bag/20230419_182853.bag")
# 画像取得可能化
pipeline = rs.pipeline()
profiles = pipeline.start(config)
device = profiles.get_device()

# %%
print(device)
mode = rs.rs400_advanced_mode(device)


# %%
import json
print(json.loads(mode.serialize_json()))

# %%
for d in rs.context().query_devices():
    print(d)