import pyrealsense2 as rs
import sys
import cv2
import numpy as np

D_HEIGHT = 720
D_WIDTH = 1280
C_HEIGHT = 360
C_WIDTH = 640
RESIZE_TO = (640, 360)

def get_color_fov(profile: rs.pipeline_profile) -> list:
    return rs.rs2_fov(profile.get_stream(rs.stream.color, -1).as_video_stream_profile().get_intrinsics())

def main():
    file_name = sys.argv[1] if len(sys.argv)>1 else "test.bag"

    config = rs.config()
    config.enable_device_from_file(file_name)
    # config.enable_stream(rs.stream.depth, D_WIDTH, D_HEIGHT, rs.format.z16, 30)
    # config.enable_stream(rs.stream.color, C_WIDTH, C_HEIGHT, rs.format.bgr8, 30)

    align_to = rs.stream.color
    align = rs.align(align_to)

    pipeline = rs.pipeline()
    profiles = pipeline.start(config)

    depth_camera, rgb_camera, _ = profiles.get_device().sensors
    # videoの場合に必要なもの
    dp = depth_camera.profiles[0].as_video_stream_profile()
    rp = rgb_camera.profiles[0]
    print(depth_camera.get_info(rs.camera_info.name)=="Stereo Module")
    print(rgb_camera.get_info(rs.camera_info.name)=="RGB Camera")
    print(dp.format(), type(dp.format()))
    print(dp.fps(), type(dp.fps()))
    print(dp.is_video_stream_profile())
    print(dp.intrinsics, type(dp.intrinsics))
    print(dp.height(), type(dp.height()))
    print(dp.intrinsics.height, type(dp.intrinsics.height))
    print(dp.width(), type(dp.width()))
    print(dp.intrinsics.width, type(dp.intrinsics.width))

    print(profiles.get_stream(rs.stream.color, -1).as_video_stream_profile())
    print(profiles.get_stream(rs.stream.depth, -1).as_video_stream_profile())
    print(get_color_fov(profiles))

    try:
        key = -1
        while key==-1:
            # frames = pipeline.wait_for_frames()
            frames = align.process(pipeline.wait_for_frames())

            depth_image = np.asanyarray(frames.get_depth_frame().get_data())
            color_image = np.asanyarray(frames.get_color_frame().get_data())
            color_image = color_image[:,:,::-1]

            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            images = np.hstack((color_image, depth_colormap))
            cv2.imshow('images', images)

            key = cv2.waitKey(1)
    finally:
        cv2.destroyAllWindows()
        pipeline.stop()


if __name__=="__main__":
    main()